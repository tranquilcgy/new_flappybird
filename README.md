#include<stdlib.h>
#include<stdio.h>
#include <conio.h>
#include <graphics.h>
#pragma comment(lib,"Winmm.lib")
#define High 600  // 游戏画面尺寸
#define Width 350

// 全局变量
IMAGE img_bk,img_bd1,img_bd2,img_bar_up1,img_bar_up2,img_bar_down1,img_bar_down2;
int bird_x,bird_y;
int bar_x;//障碍横坐标
int bar_y1,bar_y2;//两个障碍纵坐标
int barhigh;  //两个障碍之间距离



void startup()  // 数据初始化
{
    mciSendString("open E:\\background.mp3 alias bkmusic",NULL,0,NULL);//背景音乐
    mciSendString("play bkmusic repeat",NULL,0,NULL);//循环播放

    initgraph(Width, High);
    loadimage(&img_bk,"E:\\素材\\flappy bird图片音乐素材\\background.jpg");
    loadimage(&img_bd1,"E:\\素材\\flappy bird图片音乐素材\\bird1.jpg");
    loadimage(&img_bd2,"E:\\素材\\flappy bird图片音乐素材\\bird2.jpg");
    loadimage(&img_bar_up1,"E:\\素材\\flappy bird图片音乐素材\\bar_up1.gif");
    loadimage(&img_bar_up2,"E:\\素材\\flappy bird图片音乐素材\\bar_up2.gif");
    loadimage(&img_bar_down1,"E:\\素材\\flappy bird图片音乐素材\\bar_down1.gif");
    loadimage(&img_bar_down2,"E:\\素材\\flappy bird图片音乐素材\\bar_down2.gif");
    
    bird_x=50;
    bird_y=200;

    bar_x=150;
    barhigh=720;

    bar_y1=-350;


    BeginBatchDraw();
}

void clean()  // 显示画面
{
    
}    

void show()  // 显示画面
{
    putimage(0,0,&img_bk);
    putimage(bar_x,bar_y1,&img_bar_up1,NOTSRCERASE);//显示上一半的障碍物
    putimage(bar_x,bar_y1,&img_bar_up2,SRCINVERT);
    putimage(bar_x,bar_y2,&img_bar_down1,NOTSRCERASE);//显示下一半的障碍物
    putimage(bar_x,bar_y2,&img_bar_down2,SRCINVERT);
    putimage(bird_x,bird_y,&img_bd1,NOTSRCERASE);//显示小鸟
    putimage(bird_x,bird_y,&img_bd2,SRCINVERT);

    FlushBatchDraw();
    // 延时
    Sleep(50);    
}    

void updateWithoutInput()  // 与用户输入无关的更新
{
    bar_x-=2;



    if(bar_x<=-140)
    {    
        bar_y1=(rand()%270)-520;
        bar_x=350;
    }
    bar_y2=bar_y1+barhigh;

    if (bird_y<580)
        bird_y=bird_y+2;
    if (bird_x==bar_x-34&&((bird_y>bar_y2)||(bird_y<bar_y1+600)))
    {
        exit(0);
    }

    if ((bird_x<=bar_x+140&&bird_x>=bar_x-34)&&((bird_y>bar_y2+5)||(bird_y<bar_y1+595)))
    {
        printf("游戏结束\n");
        exit(0);
    }

    
    
}

void updateWithInput()  // 与用户输入有关的更新
{    
    char input;
    if (kbhit())
    {
        input=getch();
        if (input==' '&&bird_y>20)
        {
            bird_y-=60;
            mciSendString("close jpmusic",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open E:\\jump.mp3 alias jpmusic",NULL,0,NULL);
            mciSendString("play jpmusic",NULL,0,NULL);
        }
        
    }

}

void gameover()
{
    EndBatchDraw();
    closegraph();
}

int main()
{
    startup();  // 数据初始化    
    while (1)  //  游戏循环执行
    {
        clean();  // 把之前绘制的内容清除
        updateWithoutInput();  // 与用户输入无关的更新
        updateWithInput();     // 与用户输入有关的更新
        show();  // 显示新画面
    }
    gameover();     // 游戏结束、后续处理
    return 0;
}

